/*
 * LandisPlusGyr - A Landis+Gyr E350 reader
 * Copyright 2021, Raging Goblin
 * <https://gitlab.com/landisplusgyr>
 *
 * This file is part of LandisPlusGyr.
 *
 *  LandisPlusGyr is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  LandisPlusGyr is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with LandisPlusGyr. If not, see <http://www.gnu.org/licenses/>.
 */
package raging.goblin.landisplusgyr.service;

import static raging.goblin.landisplusgyr.domain.Direction.RECEIVED;
import static raging.goblin.landisplusgyr.domain.Tariff.TARIFF1;
import static raging.goblin.landisplusgyr.domain.Tariff.TARIFF2;

import java.time.OffsetDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import raging.goblin.landisplusgyr.domain.Direction;
import raging.goblin.landisplusgyr.domain.ElectricityValue;
import raging.goblin.landisplusgyr.domain.GasValue;
import raging.goblin.landisplusgyr.domain.Tariff;
import raging.goblin.landisplusgyr.domain.reader.Telegram;
import raging.goblin.landisplusgyr.repository.ElectricityValueRepository;
import raging.goblin.landisplusgyr.repository.GasValueRepository;

@Service
@Slf4j
public class DatabaseWriter {

    @Autowired
    private GasValueRepository gasReadingRepository;
    @Autowired
    private ElectricityValueRepository electricityReadingRepository;

    public void writeElectricityValues(final Telegram telegram) {
        final ElectricityValue electricityReceivedTariff1 = createElectricityValue(telegram.getDateTime(), telegram.getElectricityReceivedTariff1().getReading(), TARIFF1, RECEIVED);
        final ElectricityValue electricityReceivedTariff2 = createElectricityValue(telegram.getDateTime(), telegram.getElectricityReceivedTariff2().getReading(), TARIFF2, RECEIVED);

        try {
            electricityReadingRepository.saveAndFlush(electricityReceivedTariff1);
            electricityReadingRepository.saveAndFlush(electricityReceivedTariff2);
        } catch (final Exception e) {
            log.error("Unable to store ElectricityReadings {}, {}", electricityReceivedTariff1, electricityReceivedTariff2, e);
        }

        log.info("ElectricityReading of {} {} {} {} stored at {}",
            telegram.getElectricityReceivedTariff1(),
            telegram.getElectricityReceivedTariff2(),
            telegram.getElectricityDeliveredTariff1(),
            telegram.getElectricityDeliveredTariff2(),
            telegram.getDateTime());
    }

    public void writeGasValue(final Telegram telegram) {
        final GasValue gasValue = new GasValue();
        gasValue.setDateTime(telegram.getGasReading().getDateTime());
        gasValue.setReading(telegram.getGasReading().getReading());

        try {
            gasReadingRepository.saveAndFlush(gasValue);
        } catch (final Exception e) {
            log.error("Unable to store GasReading {}", gasValue, e);
        }

        log.info("GasReading of {} stored at {}", gasValue.getReading(), gasValue.getDateTime());
    }

    private ElectricityValue createElectricityValue(final
                                                    OffsetDateTime dateTime, final Double reading, final Tariff tariff, final Direction direction) {
        final ElectricityValue electricityValue = new ElectricityValue();
        electricityValue.setDateTime(dateTime);
        electricityValue.setReading(reading);
        electricityValue.setTariff(tariff);
        electricityValue.setDirection(direction);
        return electricityValue;
    }
}
