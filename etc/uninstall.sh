#!/bin/bash

# 
# Copyright 2017, Raging Goblin <https://raginggoblin.wordpress.com/>
# 
# This file is part of LandisPlusGyr.
# 
#  LandisPlusGyr is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  LandisPlusGyr is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with LandisPlusGyr.  If not, see <http://www.gnu.org/licenses/>.
# 

echo "Stop service"
systemctl stop landisplusgyr.service

echo "Remove service"
systemctl disable landisplusgyr.service
rm /etc/systemd/system/landisplusgyr.service
systemctl daemon-reload
systemctl reset-failed

echo "Uninstall software"
rm -rf /opt/landisplusgyr
rm /var/log/landisplusgyr.log

echo "Remove user landisplusgyr"
userdel landisplusgyr

