#!/bin/bash

# Copyright 2017, Raging Goblin <https://raginggoblin.wordpress.com/>
# 
# This file is part of LandisPlusGyr.
# 
#  LandisPlusGyr is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  LandisPlusGyr is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with LandisPlusGyr.  If not, see <http://www.gnu.org/licenses/>.
# 


echo "Create user landisplusgyr"
useradd -M -N -r landisplusgyr

echo "Install software"
mkdir -p /opt/landisplusgyr
cp landisplusgyr-1.0.jar /opt/landisplusgyr/landisplusgyr.jar
cp application.sample.properties /opt/landisplusgyr/application.properties
chown -R landisplusgyr /opt/landisplusgyr
touch /var/log/landisplusgyr.log
chown landisplusgyr /var/log/landisplusgyr.log

echo "Configure service"
cp landisplusgyr.service /etc/systemd/system/landisplusgyr.service
chmod +x /etc/systemd/system/landisplusgyr.service
systemctl enable landisplusgyr.service

echo "Start service"
systemctl restart landisplusgyr.service
