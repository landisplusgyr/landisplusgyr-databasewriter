# LandisPlusGyr Databasewriter

Read a Landis + Gyr E350 smart meter and write readings to a database. The database is fount at 
[LandisPlusGyr Database](https://gitlab.com/landisplusgyr/landisplusgyr-database)

##### How to run
In order to run this application you will need [Docker](https://www.docker.com/) and [Docker Compose](https://docs.docker.com/compose/).
Change the [docker-compose.yml](https://gitlab.com/landisplusgyr/landisplusgyr-databasewriter/-/blob/master/docker-compose.yml)
according to your own requirements.
- READING_ENABLED: When set to false the application won't read data from a serial port.
- READING_DELAY: Timespan between readings in seconds. 
- SERIAL_PORT, SERIAL_PORT_BAUDRATE, SERIAL_PORT_DATABITS, SERIAL_PORT_STOPBITS and SERIAL_PORT_PARITY: The serial port configuration to read from.
- TIMEZONE: Timezone should be set to the correct timezone, see [Wikipedia](https://en.wikipedia.org/wiki/List_of_tz_database_time_zones).
- DATABASE_HOST, DATABASE_PORT, DATABASE_NAME, DATABASE_USERNAME and DATABASE_PASSWORD: Configuration for the database connection.

Run the database using the following command:
`docker-compose -f docker-compose.yml up -d`

