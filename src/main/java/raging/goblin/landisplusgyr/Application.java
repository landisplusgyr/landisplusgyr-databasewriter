/*
 * LandisPlusGyr - A Landis+Gyr E350 reader
 * Copyright 2021, Raging Goblin
 * <https://gitlab.com/landisplusgyr>
 *
 * This file is part of LandisPlusGyr.
 *
 *  LandisPlusGyr is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  LandisPlusGyr is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with LandisPlusGyr. If not, see <http://www.gnu.org/licenses/>.
 */
package raging.goblin.landisplusgyr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

@SpringBootApplication
public class Application {

    public static void main(final String[] args) {
        new SpringApplication(Application.class).run(args);
    }

    @Bean
    public ThreadPoolTaskScheduler threadPoolTaskScheduler() {
        final ThreadPoolTaskScheduler scheduler = new ThreadPoolTaskScheduler();
        scheduler.initialize();
        return scheduler;
    }
}
