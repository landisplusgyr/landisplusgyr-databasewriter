FROM openjdk:16-slim-buster
WORKDIR /landisplusgyr
ADD target/*.jar landisplusgyr.jar
CMD ["java", "-jar", "./landisplusgyr.jar"]
